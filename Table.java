package com.getjavajob.training.web03.gorelov;

import java.io.*;
import java.lang.System;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ����� on 08.07.2015.
 */


public class Table {
    private InputStream inputStream;
    private List<Integer> maximumLenghts = new ArrayList<>();
    private List<List<String>> lines = new ArrayList<>();

    public Table(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public static void main(String[] args) {
        try {
            new Table(System.in).parse();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createTable(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        boolean isFirstLine = true;
        ArrayList<String> columns;
        Pattern pattern = Pattern.compile("\\|[\\s\\w]*");
        while ((line = reader.readLine()) != null) {
            columns = new ArrayList<>();
            Matcher matcher = pattern.matcher(line);
            int index = 0;
            while (matcher.find()) {
                String s = matcher.group();
                s = s.substring(1);
                s = s.trim();
                columns.add(s);
                if (isFirstLine) {
                    maximumLenghts.add(s.length());
                } else {
                    if (maximumLenghts.get(index) < s.length()) {
                        maximumLenghts.remove(index);
                        maximumLenghts.add(index, s.length());
                    }
                }
                ++index;
            }
            columns.remove(columns.size() - 1);
            lines.add(columns);
            isFirstLine = false;
        }
    }

    private String getString(List<List<String>> lines){
        StringBuilder[] newLines = new StringBuilder[maximumLenghts.size()];
        for (int i = 0; i < newLines.length; i++) {
            newLines[i] = new StringBuilder();
        }

        for (int i = 0; i < lines.size(); ++i) {
            for (int j = 0; j < maximumLenghts.size() - 1; ++j) {
                newLines[i].append("|");
                int countSpaces = (maximumLenghts.get(j) - lines.get(i).get(j).length()) / 2;
                for (int x = 0; x < countSpaces; ++x) {
                    newLines[i].append(" ");
                }
                newLines[i].append(lines.get(i).get(j));
                countSpaces += (maximumLenghts.get(j) - (lines.get(i).get(j)).length()) % 2;
                for (int x = 0; x < countSpaces; ++x) {
                    newLines[i].append(" ");
                }
            }
            newLines[i].append("|");
        }
        for (int i = 1; i < newLines.length; ++i) {
            newLines[0].append("\n").append(newLines[i]);
        }
        return newLines[0].toString();
    }

    public void parse() throws IOException {
        createTable(inputStream);
        System.out.print(getString(lines));
    }
}
