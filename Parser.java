package com.getjavajob.training.web03.gorelov;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by ����� on 08.07.2015.
 */


public class Parser {
    InputStream inputStream;

    public Parser(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public static void main(String[] args) {

        try {
            Parser parser = new Parser(System.in);
            parser.parse();
        } catch (ParserConfigurationException | SAXException | TransformerException | IOException e) {
            e.printStackTrace();
        }
    }

    private List<Project> createTree(Document document) {
        List<Project> projects = new ArrayList<>();
        NodeList nodeList = document.getDocumentElement().getChildNodes();
        for (int i = 0; i < nodeList.getLength(); ++i) {
            Node node = nodeList.item(i);
            if (node instanceof Element) {
                Project project = new Project();
                project.setName(((Element) node).getAttribute("name"));
                NodeList childNodes = node.getChildNodes();
                for (int j = 0; j < childNodes.getLength(); ++j) {
                    Node cNode = childNodes.item(j);
                    if (cNode instanceof Element) {
                        Member member = new Member();
                        member.setRole(((Element) cNode).getAttribute("role"));
                        member.setName(((Element) cNode).getAttribute("name"));
                        project.addMember(member);
                    }
                }
                projects.add(project);
            }
        }
        return projects;
    }

    private Map<Member, List<Role>> transformTree(List<Project> projects) {
        Map<Member, List<Role>> memberMap = new TreeMap<>();

        for (Project project : projects) {
            for (Member member : project.getMembers()) {
                if (memberMap.containsKey(member)) {
                    Role role = new Role();
                    role.setName(member.getRole());
                    role.setProject(project.getName());
                    memberMap.get(member).add(role);
                } else {
                    ArrayList<Role> roles = new ArrayList<>();
                    Role role = new Role();
                    role.setName(member.getRole());
                    role.setProject(project.getName());
                    roles.add(role);
                    memberMap.put(member, roles);
                }
            }
        }
        return memberMap;
    }

    private Document getXML(Map<Member, List<Role>> memberMap) throws ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = factory.newDocumentBuilder();
        Document document = documentBuilder.newDocument();
        Element rootElement = document.createElement("members");
        for (Member member : memberMap.keySet()) {
            Element element = document.createElement("member");
            element.setAttribute("name", member.getName());
            for (Role role : memberMap.get(member)) {
                Element elementRole = document.createElement("role");
                elementRole.setAttribute("name", role.getName());
                elementRole.setAttribute("project", role.getProject());
                element.appendChild(elementRole);
            }
            rootElement.appendChild(element);
        }
        document.appendChild(rootElement);
        return document;
    }

    public void parse() throws ParserConfigurationException, IOException, SAXException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = factory.newDocumentBuilder();
        Document document = documentBuilder.parse(inputStream);

        List<Project> projectsTree = createTree(document);
        Map<Member, List<Role>> membersTree = transformTree(projectsTree);
        document = getXML(membersTree);

        Transformer t = TransformerFactory.newInstance().newTransformer();
        t.setOutputProperty(OutputKeys.METHOD, "xml");
        t.setOutputProperty(OutputKeys.INDENT, "yes");
        t.transform(new DOMSource(document), new StreamResult(System.out));
    }

    class Project {
        private String name;
        private List<Member> members = new ArrayList<>();

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Member> getMembers() {
            return members;
        }

        public void addMember(Member member) {
            members.add(member);
        }
    }

    class Member implements Comparable<Member> {
        String role;
        String name;

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public int compareTo(Member o) {
            return name.compareTo(o.getName());
        }

    }

    class Role {
        String name;

        String project;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProject() {
            return project;
        }

        public void setProject(String project) {
            this.project = project;
        }

    }
}
