package com.getjavajob.training.web03.gorelov;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ����� on 08.07.2015.
 */


public class Wiki {
    private static final String UL_TAG_START = "<ul>";
    private static final String UL_TAG_END = "</ul>";
    private static final String LI_TAG_START = "<li>";
    private static final String LI_TAG_END = "</li>";
    private static final String P_TAG_START = "<p>";
    private static final String P_TAG_END = "</p>";
    private static final String STR_START = "*";
    private static final String LINE_SEPARATOR = "\n";

    private InputStream inputStream;

    public Wiki(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public static void main(String[] args) {
        try {
            new Wiki(System.in).parse();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<String> readText(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        List<String> lines = new ArrayList<>();
        String line;
        Pattern pattern = Pattern.compile("\\s+");
        while ((line = reader.readLine()) != null) {
            line = line.trim();
            Matcher matcher = pattern.matcher(line);
            line = matcher.replaceAll(" ");
            if (!"".equals(line)) {
                lines.add(line);
            }
        }
        return lines;
    }

    public void parse() throws IOException {

        List<String> lines = readText(inputStream);

        boolean lastOperation = true;
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < lines.size() - 1; ) {
            if (i < lines.size() - 1 && lines.get(i).startsWith(STR_START) && lines.get(i + 1).startsWith(STR_START)) {
                result.append(UL_TAG_START).append(LINE_SEPARATOR);
                result.append(LI_TAG_START).append(lines.get(i++).substring(1).trim()).append(LI_TAG_END).append(LINE_SEPARATOR);
                while (i < lines.size() - 1 && lines.get(i).startsWith(STR_START)) {
                    result.append(LI_TAG_START).append(lines.get(i++).substring(1).trim()).append(LI_TAG_END).append(LINE_SEPARATOR);
                }
                if (i < lines.size() - 2) {
                    result.append(UL_TAG_END).append(LINE_SEPARATOR);
                }
                lastOperation = true;
            } else {
                result.append(P_TAG_START);
                result.append(lines.get(i++));

                while (i < lines.size() - 1 && !(lines.get(i).startsWith(STR_START) && lines.get(i + 1).startsWith(STR_START))) {
                    result.append(" ").append(lines.get(i++));
                }
                if (i < lines.size() - 2) {
                    result.append(P_TAG_END).append(LINE_SEPARATOR);
                }
                lastOperation = false;
            }
        }

        if (!lastOperation) {
            result.append(" ").append(lines.get(lines.size() - 1)).append(P_TAG_END).append(LINE_SEPARATOR);
        } else {
            result.append(LI_TAG_START).append(lines.get(lines.size() - 1).substring(1).trim()).append(LI_TAG_END).append(LINE_SEPARATOR);
            result.append(UL_TAG_END).append(LINE_SEPARATOR);
        }
        System.out.print(result);
    }
}
